package com.cschool.project;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    // just an example
    @Test
    void testAssertArrayEquals() {
        // given
        int[] ints1 = {1, 2, 3};
        int[] ints2 = {1, 2, 3};

        //nie zadziała!
        // assertEquals(ints1, ints2);

        assertArrayEquals(ints1, ints2);
    }

    @Test
    void testIfCreatedOrderListIsEmptyAfterCreation() {
        //given
        Order order = new Order();
        //then - 4 różne metody
        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));
    }

    @Test
    void checkIfAfterAddingFirstMealOrderSizeIsEqualTo1() {
        //given
        Order order = new Order();
        Meal meal1 = new Meal(17, "cheeseburger");
        //when
        order.addMealToOrder(meal1);
        //then
        assertThat(order.getMeals().size(), equalTo(1));
    }

    @Test
    void checkIfOrderContainsMealAfterAddingIt() {
        Meal meal1 = new Meal(10, "burger");

        Order order = new Order();
        order.addMealToOrder(meal1);

        assertThat(order.getMeals(), contains(meal1));


    }

    @Test
    void checkIfOrderContainsMealAfterAddingIt2() {
        Meal meal1 = new Meal(10, "burger");

        Order order = new Order();
        order.addMealToOrder(meal1);

        assertThat(order.getMeals(), hasItem(meal1));
        assertThat(order.getMeals().get(0).getPrice(), equalTo(10));

    }

    @Test
    void checkIfAfterDeletingMealListDoesntContainIt() {
        Meal meal1 = new Meal(10, "burger");

        Order order = new Order();
        order.addMealToOrder(meal1);
        order.removeMealFromOrder(meal1);

        assertThat(order.getMeals(), not(hasItem(meal1)));
    }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder() {
        Meal meal1 = new Meal(10, "burger");
        Meal meal2 = new Meal(15, "pizza");

        Order order = new Order();
        order.addMealToOrder(meal1);
        order.addMealToOrder(meal2);

        assertThat(order.getMeals(), contains(meal1, meal2));
        assertThat(order.getMeals(), containsInAnyOrder(meal2, meal1));
        assertThat(order.getMeals(), hasItems(meal1, meal2));
    }

    @Test
    void twoOrdersShoudbeIdenticalIfContainingSameMeals() {
        Meal meal1 = new Meal(10, "burger");
        Meal meal2 = new Meal(15, "pizza");
        Meal meal3 = new Meal(22, "pierogi");

        List<Meal> mealList1 = Arrays.asList(meal1, meal2);
        List<Meal> mealList2 = Arrays.asList(meal1, meal2);
        List<Meal> mealList3 = Arrays.asList(meal2, meal1);
        List<Meal> mealList4 = Arrays.asList(meal2, meal1, meal3);

        Order order1 = new Order();
        Order order2 = new Order();

        order1.addMealToOrder(mealList1);

        order2.addMealToOrder(mealList2);

        assertThat(order1.getMeals(), is(order2.getMeals()));

        // w domu: jak porównać listy zawierające te same obiekty ale w innej kolejności
    }

}