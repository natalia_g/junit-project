package com.cschool.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.function.Executable;

class MealTest {

    @Test
    void getDiscountPrice_givenCorrectValues() {
        //given
        Meal meal = new Meal(15);

        //when
        int discountedPrice = meal.getDiscountPrice(5);

        //then
        Assertions.assertEquals(10, discountedPrice);
    }

    @Test
    void givenPriceLowerThanDiscount_shouldThrowException() {
        //given
        Meal meal = new Meal(15);

        //when
        Executable executable = () -> meal.getDiscountPrice(20);

        //then
        Assertions.assertThrows(IllegalArgumentException.class, executable);

    }

    @Test
    void referencesToDifferentObjects_ShouldNotBeEqual(){
        //given
        Meal meal1 = new Meal(10);
        Meal meal2 = new Meal(20);
        Meal meal3 = new Meal(20);

        //when, than
        Assertions.assertNotEquals(meal3, meal2);
//      nie powinno się robić dwóch testów w 1 metodzie testowej
        Assertions.assertEquals(meal2, meal3);
    }

    @Test
    void twoMealsShouldBeEqual_WhenPriceAndNameAreSame(){
        //given
        Meal meal1 = new Meal(10, "buger");
        Meal meal2 = new Meal(10, "buger");

        //when, than
        Assertions.assertEquals(meal1, meal2);

    }

}