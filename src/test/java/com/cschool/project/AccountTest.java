package com.cschool.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldBeActive() {
        Account account = new Account();
        Assertions.assertFalse(account.isActive());

        assertThat(account.isActive(), equalTo(false));
        assertThat(account.isActive(), is(false));
    }

    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation() {
        Account account = new Account();
        account.activate();
        Assertions.assertTrue(account.isActive());
    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull () {
        // given
        Account account = new Account();

        //when, then
        Assertions.assertNull(account.getDefaultDeliveryAddress());
        assertThat(account.getDefaultDeliveryAddress(), nullValue());

    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSet() {
        // given
        Account account = new Account();
        Address address = new Address("Jasna", "13b");
        account.setDefaultDeliveryAddress(address);
        //when, then
        Assertions.assertNotNull(account.getDefaultDeliveryAddress());
        assertThat(account.getDefaultDeliveryAddress(), notNullValue());

    }

    @Test
    void givenCorrectEmailAddressShouldReturnNotNUllValue() {
        //given
        Account account = new Account();
        //when
        account.setEmailAddress("james@gmail.com");
        // then
        assertThat(account.getEmailAddress(), notNullValue());

    }

    @Test
    void givenWrongEmailAddressShouldThrowException() {
        //given
        Account account = new Account();

        // then
        assertThrows(IllegalArgumentException.class, () -> account.setEmailAddress("jamesgmail.com"));

    }

    @Test
    void givenTooShortEmailAddressShouldThrowException() {
        //given
        Account account = new Account();

        // then
        assertThrows(IllegalArgumentException.class, () -> account.setEmailAddress("james.com"));

    }

    @ParameterizedTest
    @ValueSource(strings = {"james@gmail.com", "kasia@onet.pl", "admin@wp.pl"})
    void givenCorrectEmailAddressShouldReturnNotNUllValue2(String strings) {
        Account account = new Account();
        account.setEmailAddress(strings);
        assertThat(account.getEmailAddress(), notNullValue());
    }

//    @ParameterizedTest
//    @ValueSource(strings = {"jas@gm.com", "k@onet.pl", "ad@wp.pl"})
//    void givenTooShortEmailAddressShouldThrowException2(String strings){
//        Account account = new Account();
//        account.setEmailAddress(strings);
//        assertThat(account.getEmailAddress(), );
//    }
}