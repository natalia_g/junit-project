package com.cschool.project;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    User user;

    @BeforeEach
    void setUser() {
        user = new User();
    }

    @Test
    void checkIfNewUserHasNullName() {
        assertNull(user.getName());
    }

    @Test
    void givenAgeOver18ShouldSetAgeCorrectly() {
        user.setAge(20);
        assertThat(user.getAge(), notNullValue());
    }

    @Test
    void givenAgeUnder18ShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> {
            user.setAge(15);
        });
    }

    @Test
    void givenNameLongerThan4CharactersShouldSetNameCorrectly() {
        user.setName("Paweł");
        assertThat(user.getName(), notNullValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"abd", "xyz", "aaa"})
    void checkIfGivenNameShorterThan5CharactersThrowsException(String string) {
        assertThrows(IllegalArgumentException.class, () -> {
            user.setName(string);
        });
    }

}