import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class SimpleCalculatorTest {

    @Test
    void add() {

        Assertions.assertEquals(4, SimpleCalculator.add(1, 3));

    }

    @Test
    void multiply() {

        Assertions.assertEquals(6, SimpleCalculator.multiply(2, 3));

    }

    @Test
    void divide() {

        Assertions.assertEquals(2.5, SimpleCalculator.divide(10, 4));

    }

    @Test
    void divide_throwsException() {

        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            SimpleCalculator.divide(5, 0);
        });

    }
}