import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExceptionTestExample {

    private boolean compare(String secret){
        if(secret == null || secret.isEmpty()){
            throw new IllegalArgumentException("Secret is empty!");
        }
        return "Hello".equalsIgnoreCase(secret);
    }

    @Test
    public void compare_givenEmptySecret_shouldThrowException(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            compare("");
        });
    }

    @Test
    public void compare_givenNullSecret_shouldThrowException(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            compare(null);
        });
    }

    @Test
    public void compare_givenWrongSecret_shouldReturnFalse(){
        boolean compare = compare("Witaj świecie");
        Assertions.assertFalse(compare);
    }

    @Test
    public void compare_givenCorrectSecret_shouldReturnFalse(){
        boolean compare = compare("Hello");
        Assertions.assertTrue(compare);
    }
}
