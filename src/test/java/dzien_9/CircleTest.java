package dzien_9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    Circle circle;

    @BeforeEach
    void setDataToTest(){
        circle = new Circle(1);
    }

    @Test
    void checkIfCircleAreaIsCalculatedCorrectly(){
        Assertions.assertEquals(3.14, circle.getArea());
    }

    @Test
    void checkIfCircleCircumferenceIsCalculatedCorrectly(){
        Assertions.assertEquals(6.28, circle.getCircumference());
    }


}