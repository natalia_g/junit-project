package dzien_9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

class FactorialTest {

    @Test
    void testFactorialLong(){
        Assertions.assertEquals(24, Factorial.getFactorialLong(4));
        Assertions.assertEquals(40320, Factorial.getFactorialLong(8));
    }

    @Test
    void testFactorialBigInteger(){
        Assertions.assertEquals(BigInteger.valueOf(24), Factorial.getFactorialBigInteger(4));
        Assertions.assertEquals(BigInteger.valueOf(40320), Factorial.getFactorialBigInteger(8));
    }

    @ParameterizedTest
    @MethodSource("factorialDataProvider")
    void testFactorialBigIntegerParametrized(int number, BigInteger calculatedOutcome){
        BigInteger valueOfFactorial = Factorial.getFactorialBigInteger(number);
        Assertions.assertEquals(calculatedOutcome, valueOfFactorial);
    }

    private static Stream factorialDataProvider(){
        return Stream.of(
                Arguments.of(5, BigInteger.valueOf(120)),
                Arguments.of(8, BigInteger.valueOf(40320)),
                Arguments.of(20, BigInteger.valueOf(2432902008176640000L))
        );
    }
}