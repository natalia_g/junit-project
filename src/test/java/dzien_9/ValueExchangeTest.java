package dzien_9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class ValueExchangeTest {
    ValueExchange valueExchange;

    @BeforeEach
    void initializeConverter() {
        valueExchange = new ValueExchange("X", 4.25);
    }

    @Test
    void testChangeZlotyToEuro() {
        Assertions.assertEquals(23.53, valueExchange.changeZlotyToEuro(100));
    }

    @Test
    void testChangeEuroToZloty() {
        Assertions.assertEquals(425, valueExchange.changeEuroToZloty(100));
    }

    @ParameterizedTest
    @MethodSource("exchangeEtoZDataProvier")
    void testChangeZlotyToEuroParametrized(double zloties){

    }


    private Stream exchangeEtoZDataProvier(){
        return Stream.of(
                Arguments.of(425, 100),
                Arguments.of(4.25, 1),
                Arguments.of(69.28, 16.30),
                Arguments.of(24.5, 5.765)
        );
    }
}