package dzien_9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CylinderTest {

    Circle circle;
    Cylinder cylinder;

    @BeforeEach
    void setDataToTest(){
        circle = new Circle(1);
        cylinder = new Cylinder(1, circle);
    }

    @Test
    void checkIfCylinderAreaIsCalculatedCorrectly(){
        Assertions.assertEquals(12.56, cylinder.getArea());
    }

    @Test
    void checkIfCylinderCircumferenceIsCalculatedCorrectly(){
        Assertions.assertEquals(3.14, cylinder.getVolume());
    }



}