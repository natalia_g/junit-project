package lambda.exercises;

import java.util.Arrays;
import java.util.List;

public class LambdaExample2 {

    public static void main(String[] args) {

        Sum sum = (a, b) -> a * b + 10;
        System.out.println(sum.calculate(2, 3));

        StringInterface stringInterface = (string) -> string.concat("abc");
        System.out.println(stringInterface.addABC("janina"));

        StringInterface stringInterface1 = string -> string.concat(string.substring(0,1).toUpperCase().concat(string.substring(1)));
        System.out.println(stringInterface1.addABC("janina"));

        StringInterface stringInterface2 = string -> {
            String upperCase = string.toUpperCase();
            return string.concat(upperCase);
        };
        System.out.println(stringInterface2.addABC("janina"));

        PowerOn powerOn = () -> {
            //System.out.println(SimpleCalculator.add(3,5)
            //);
        };
        powerOn.activate();



    }

}
