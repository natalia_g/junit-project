package lambda.exercises;

@FunctionalInterface
public interface PowerOn {
    void activate();
}
