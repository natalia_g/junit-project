package lambda.exercises;

@FunctionalInterface
public interface Sum {

    int calculate (int a, int b);

}
