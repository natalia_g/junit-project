package lambda.exercises;

@FunctionalInterface
public interface Modifier {
    String modify(String string);
}
