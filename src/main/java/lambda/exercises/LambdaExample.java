package lambda.exercises;

public class LambdaExample {

    public static void main(String[] args) {

        SumImpl sumImpl = new SumImpl();
        Sum sumImpl2 = new SumImpl();

        System.out.println(sumImpl.calculate(2, 6));

        Sum sumInstance = new Sum() {
            @Override
            public int calculate(int a, int b) {
                return 0;
            }
        };

        System.out.println(sumInstance.calculate(2, 6));

        Sum sumInstance2 = new Sum() {
            @Override
            public int calculate(int a, int b) {
                return a + b + 8 ;
            }
        };

        // wyrażenie lambda
        Sum sumInstance3 = (a, b) -> a + b + 2;

        System.out.println(sumInstance3.calculate(2, 3));

    }

}
