public class SimpleCalculator {

    public static int add(int a, int b){
        return a + b;
    }

    public static int multiply(int a, int b){
        return a * b;
    }

    public static double divide(int a, int b){
        if( b == 0){
            throw new IllegalArgumentException("Don't divide by 0!");
        }
        return (double) a / b;
    }

    public static void main(String[] args) {
        System.out.println("Simple Calculator");
    }

}
