package com.cschool.project;

public class User {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 5) {
            throw new IllegalArgumentException("Name should has at least 5 characters.");
        } else {
            this.name = name;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 18) {
            throw new IllegalArgumentException("User has to be adult.");
        } else {
            this.age = age;
        }
    }
}
