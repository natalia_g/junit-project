package com.cschool.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order {
    private List<Meal> meals = new ArrayList<>();

    public void addMealToOrder(Meal mealToAdd) {
        meals.add(mealToAdd);
    }

    public void addMealToOrder(List<Meal> mealListToAdd) {
        meals.addAll(mealListToAdd);
    }

    public void removeMealFromOrder(Meal mealToRemove) {
        meals.remove(mealToRemove);
    }

    public List<Meal> getMeals() {
        return meals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(getMeals(), order.getMeals());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeals());
    }
}
