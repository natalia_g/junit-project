package com.cschool.project;

public class Account {
    private boolean active;
    private Address defaultDeliveryAddress;
    private String emailAddress;

    public Account() {
    }

    public Account(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        // check if contains @
        // if (emailAddress.matches(".*[@].*")) {
        if (emailAddress.contains("@") && emailAddress.length() > 10) {
            this.emailAddress = emailAddress;
        } else {
            throw new IllegalArgumentException("Email must contain @ character and be longer than 10");
        }
    }

    public Address getDefaultDeliveryAddress() {
        return defaultDeliveryAddress;
    }

    public void setDefaultDeliveryAddress(Address defaultDeliveryAddress) {
        this.defaultDeliveryAddress = defaultDeliveryAddress;
    }

    public static void main(String[] args) {
        Account account1 = new Account();
        System.out.println(account1.isActive());

        Account account2 = new Account();
        System.out.println(account2.getDefaultDeliveryAddress());
    }
}
