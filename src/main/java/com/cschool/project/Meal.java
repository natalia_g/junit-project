package com.cschool.project;

import java.util.Objects;

public class Meal {
    private int price;
    private String name;

    public Meal(int price) {
        this.price = price;
    }

    public Meal(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscountPrice(int discount){
        if(discount > this.price) {
            throw new IllegalArgumentException("Discount cannot be bigger than price");
        }
        return this.price - discount;
    }

    public static void main(String[] args) {

        Meal meal3 = new Meal(10);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meal)) return false;
        Meal meal = (Meal) o;
        return getPrice() == meal.getPrice() &&
                Objects.equals(name, meal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrice(), name);
    }
}
