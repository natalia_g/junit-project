package dzien_9;

import org.decimal4j.util.DoubleRounder;

public class ValueExchange {
    private String name;
    private double exchangeRate;

    public ValueExchange(String name, double exchangeRate) {
        this.name = name;
        this.exchangeRate = exchangeRate;
    }

    public double changeZlotyToEuro(double zlotyToChange){
       double result = zlotyToChange / exchangeRate;
       return DoubleRounder.round(result, 2);
    }

    public  double changeEuroToZloty(double euroToChange){
        double result = euroToChange * exchangeRate;
        return DoubleRounder.round(result, 2);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public static void main(String[] args) {
        ValueExchange valueExchange1 = new ValueExchange("Big Szmal", 4.25);
        ValueExchange valueExchange2 = new ValueExchange("Najlepszy Deal", 4.38);

        System.out.println(valueExchange1.changeZlotyToEuro(100));
        System.out.println(valueExchange2.changeZlotyToEuro(100));
        System.out.println(valueExchange1.changeEuroToZloty(100));
        System.out.println(valueExchange1.changeEuroToZloty(1));
        System.out.println(valueExchange1.changeEuroToZloty(16.30));
        System.out.println(valueExchange1.changeEuroToZloty(5.765));
    }

}

//masz konwerter walut który przelicza kurs euro na złotówki i złotówki na euro – napisz testy które potwierdzą
// prawidłowość konwersji walut. Możesz użyć testów jednostkowych lub parametryzowanych