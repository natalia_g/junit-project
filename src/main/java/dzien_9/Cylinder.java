package dzien_9;

public class Cylinder {
    private double height;
    private Circle circle;

    public Cylinder(double height, Circle circle) {
        this.height = height;
        this.circle = circle;
    }

    public double getArea(){
        return this.circle.getArea() * 2 + this.circle.getCircumference();
    }

    public double getVolume(){
        return this.circle.getArea() * this.height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public static void main(String[] args) {
        Circle circle = new Circle(1);
        Cylinder cylinder = new Cylinder(1, circle);
        System.out.println(cylinder.getArea());
        System.out.println(cylinder.getVolume());
    }
}
