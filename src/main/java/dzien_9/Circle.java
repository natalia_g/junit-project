package dzien_9;

public class Circle {
    private double radius;
    final double PI = 3.14;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getCircumference(){
        return 2 * PI * this.radius;
    }

    public double getArea(){
        return PI * this.radius * this.radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public static void main(String[] args) {
        Circle circle = new Circle(1);
        System.out.println(circle.getArea());
        System.out.println(circle.getCircumference());

    }
}
