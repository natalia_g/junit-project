package dzien_9;

import java.math.BigInteger;

public class Factorial {

       public static BigInteger getFactorialBigInteger(int number) {
        BigInteger factorial = BigInteger.valueOf(1);
        for(int i = 1; i <= number; i++){
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }

    public static long getFactorialLong (int number) {
        long factorial = 1;
        for(int i = 1; i <= number; i++){
            factorial *= i;
        }
        return factorial;
    }

    public static void main(String[] args) {
        System.out.println(getFactorialBigInteger(5));
        System.out.println(getFactorialBigInteger(21));

    }



}
